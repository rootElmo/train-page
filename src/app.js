const express = require('express')
const fs = require('fs')
const app = express()
const port = 8080
/*
fs.readFile('../views/index.html', function (err, html) {
	if (err) {
		throw err
	}
	app.get('../', (req, res) => {
		res.sendFile(__dirname, '../views/index.html')
	})
})
*/
app.get('/index.html', (req, res) => {
	res.sendFile(__dirname + '/views/index.html')
})

app.listen(port, () => {
	console.log('Running at port ' + port)
})



