# Train Page

This is a solution for the **A Project in another language** assignment in the CPP Embedded Development Bootcamp. The application is a simple **NodeJS** app which displays a HTML page to the user.

## Table of Contents

 - [Requirements](#requirements)
 - [Usage](#usage)
 - [Notes](#notes)
 - [Maintainers](#maintainers)
 - [Contributing](#contributing)
 - [License](#license)

## Requirements

Requires `nodejs` and `npm`.

	$ sudo apt install nodejs npm

## Usage

	$ npm i # Installs missing node modules
	$ node ./src/app.js

You should now have the page available to you on your browser if everything works.

Go to page **localhost/index.html** or **127.0.0.1:8080/index.html**

## Notes

Be aware, that for this app to work, you need to open port **8080** on your system, or edit the **app.js** file.

## Maintainers

[Elmo Rohula @rootElmo](https://gitlab.com/rootElmo)

## Contributing

## License

Copyright 2022, Elmo Rohula
