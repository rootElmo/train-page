FROM node:10.19.0

RUN mkdir -p /app/src

WORKDIR /app/src/

COPY package.json .

RUN npm install

COPY . .

EXPOSE 8080

ENTRYPOINT ["node", "./src/app.js"]
